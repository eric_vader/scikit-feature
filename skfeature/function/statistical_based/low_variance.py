from sklearn.feature_selection import VarianceThreshold


def low_variance_feature_selection(X, threshold):
    """
    This function implements the low_variance feature selection (existing method in scikit-learn)

    Input
    -----
    X: {numpy array}, shape (n_samples, n_features)
        input data
    p:{float}
        parameter used to calculate the threshold(threshold = p*(1-p))

    Output
    ------
    X_new: {numpy array}, shape (n_samples, n_selected_features)
        data with selected features
    """
    sel = VarianceThreshold(threshold)
    X_t = sel.fit_transform(X)
    X_i_transposed = sel.inverse_transform(X_t).T
    n_feats = len(X[0])
    bit_vec = [True] * n_feats
    for i in range(n_feats):
        col_i_T = X_i_transposed[i]
        is_i_all_zeros = not col_i_T.any()
        if is_i_all_zeros:
            bit_vec[i] = False
    return bit_vec
