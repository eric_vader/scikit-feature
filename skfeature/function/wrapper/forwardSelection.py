import numpy as np

def forward_selection(X, y, num_feats_sel, ml, **kwargs):
    """
    This function implements the forward feature selection algorithm based on decision tree
    Input
    -----
    X: {numpy array}, shape (n_samples, n_features)
        input data
    y: {numpy array}, shape (n_samples, )
        input class labels
    num_feats_sel: {int}
        number of selected features
    Output
    ------
    F: {numpy array}, shape (n_features,)
        index of selected features
    """

    n_samples, n_features = X.shape

    # selected feature set, initialized to be empty
    F = []
    count = 0
    while count < num_feats_sel:
        max_acc = 0
        for i in range(n_features):
            if i not in F:
                F.append(i)
                X_tmp = X[:, F]
                acc = ml.train_eval(X_tmp,y).mean()
                F.pop()
                # record the feature which results in the largest accuracy
                if acc > max_acc:
                    max_acc = acc
                    idx = i
        # add the feature which results in the largest accuracy
        F.append(idx)
        count += 1
    return np.array(F)

